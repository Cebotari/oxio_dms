from django.contrib import messages
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden

from .forms import AddKeyForm
from .models import Key

def home_page(request):
    return render(request,'home.html',{ 'nbar':'home'})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', { 'form': form, 'nbar':'signup' })

def profile(request):
    if request.user.is_authenticated:
        return render(request,'profile.html',{ 'nbar':'profile' })
    else:
        return redirect('/accounts/login/')

def add_key(request):
    if request.user.is_authenticated and request.method == 'POST':
        form = AddKeyForm(request.POST)
        if form.is_valid():
            new_key=Key(
                key_name = form.cleaned_data['key_name'],
                key_contents = form.cleaned_data['key_contents'],
                key_user = request.user
            )
            new_key.save()
            return redirect('/keys/') # To redo using key_id 
    elif request.user.is_authenticated:
        form = AddKeyForm()
        current_user = request.user
        users_keys = Key.objects.filter(key_user_id=current_user.id)
        return render(request, 'add_key.html',{'form':form, 'users_keys':users_keys, 'nbar':'addkey' })
    else:
        return redirect('/accounts/login/')

def remove_key(request, key_id):
    current_key = Key.objects.get(id=key_id)
    if request.user.is_authenticated and request.user == current_key.key_user:
        current_key.delete()
        return redirect('/keys/')
    else:
        return HttpResponseForbidden()

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('/accounts/password_change/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user).as_p()
    return render(request, 'registration/password_change.html', {
        'form': form,
        'nbar': 'pchange'
    })
