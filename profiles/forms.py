from .models import Key
from django import forms

class AddKeyForm(forms.ModelForm):
    class Meta:
        model = Key
        fields = ('key_name','key_contents')
