from document import crypto_settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

import gnupg
import pdb

gpg = gnupg.GPG(gnupghome=crypto_settings.GPG_HOME)

class Key(models.Model):

    key_name = models.CharField(max_length=250)
    key_contents = models.TextField()
    key_creation = models.DateTimeField(default=timezone.now)
    key_user = models.ForeignKey(User, on_delete=models.CASCADE)
    key_is_registered = models.BooleanField( default = False)
    key_fingerprints = models.TextField(default='')

    def save(self):
        import_result = gpg.import_keys(self.key_contents)
        if import_result.count == 1:
            self.key_fingerprints =  import_result.fingerprints[0]
            self.key_is_registered = True
        super().save()
        return import_result

    def delete(self):
        if len(self.key_fingerprints)>0 :
            delete_result = gpg.delete_keys(self.key_fingerprints)
        super().delete()

