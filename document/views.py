import pdb

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseForbidden
from django.core.files.storage import FileSystemStorage
from django.forms import ModelMultipleChoiceField
from django.contrib.auth.models import User
#from django.core.exceptions import 

from . import forms
from . import models

def document(request):
    if request.user.is_authenticated:
        users_documents = models.Document.objects.filter(
            document_user_id = request.user.id
        )
        return render(request, 'document.html',{
            'users_documents':users_documents,
            'nbar': 'documents'
        })
    else:
        return redirect('/accounts/login/')

def document_create(request):
    if request.method == 'POST' and request.user.is_authenticated:
        document_form = forms.CreateDocumentForm(request.POST)
        if document_form.is_valid():
            new_document = models.Document(
                document_name = document_form.cleaned_data['document_name'],
                document_user = request.user
            )
            new_document.save()
        return redirect('/document/')
    else:
        document_form = forms.CreateDocumentForm()
        return render(request,'document_create.html',{
            'document_form':document_form,
            'nbar': 'documents'
        })

def document_info(request, document_id):
    current_document = models.Document.objects.get(pk=document_id)
    allowed_users = current_document.document_shared_to_users.all()
    last_revision_link = current_document.last_revision_download()
    document_comments = current_document.get_comments()
    add_comment_form = forms.AddCommentForm(request.POST)
    is_document_owner = (current_document.document_user == request.user)
    return render(request,'document_info.html',{
        'document':current_document,
        'last_revision_download':last_revision_link,
        'shared_users':current_document.document_shared_to_users.all(),
        'nbar': 'documents',
        'comments':document_comments,
        'add_comment_form':add_comment_form,
        'is_document_owner':is_document_owner
    })

def add_comment(request, document_id):
    if request.method == 'POST' and request.user.is_authenticated:
        current_form = forms.AddCommentForm(request.POST)
        if current_form.is_valid():
            new_comment = models.DocumentComment (
                comment_contents = current_form.cleaned_data['comment_contents'],
                comment_document = models.Document.objects.get(pk = document_id),
                comment_user = request.user
            )
            new_comment.save()
        return redirect("/document/info/"+str(document_id)+'/')
    else:
        return redirect("/accounts/login/")

def document_delete(request, document_id):
    models.Document.objects.get(pk = document_id).delete()
    return redirect('/document/')

def add_revision(request, document_id):
    if request.method == 'POST' and request.user.is_authenticated:
        revision_form = forms.CreateRevisionForm(request.POST, request.FILES)
        if revision_form.is_valid():
            current_document = models.Document.objects.get( pk = document_id )
            if current_document.document_is_checked_out == False:
                myfile = request.FILES['revision_contents']
                myfile_signature = request.FILES['revision_signature']
                created_revision = models.Revision(
                    revision_document = current_document,
                    revision_contents = myfile,
                    revision_signature = myfile_signature,
                    revision_user = request.user
                )
                created_revision.save()
                created_revision.verify_signature()
        return redirect('/document/info/'+str(document_id)+'/')
    else:
        revision_form = forms.CreateRevisionForm(request.POST)
        return render(request,'revision_create.html',{
            'revision_form':revision_form,
            'nbar': 'documents'
        })

def document_share(request, document_id):
    if request.method == 'POST' and request.user.is_authenticated:
        current_form = forms.DocumentShareForm(request.POST)
        if current_form.is_valid():
            current_document = models.Document.objects.get(pk=document_id)
            for user in current_form.cleaned_data['user_field']:
                current_document.send_to(user)
        return redirect('/document/info/'+str(document_id)+'/')
    else:
        current_form = forms.DocumentShareForm()
        return render(request, 'document_share.html', {
            'share_form':current_form,
            'nbar': 'documents'
        })

def document_check_out(request, document_id):
    current_document = models.Document.objects.get( pk=document_id )
    if request.user != current_document.document_user:
        return HttpResponseForbidden()
    else:
        current_document.check_out(request.user)
        return redirect('/document/info/'+str(document_id)+'/')

def document_check_in(request, document_id):
    current_document = models.Document.objects.get( pk=document_id )
    if request.user != current_document.document_user:
        return HttpResponseForbidden()
    else:
        current_document.check_in(request.user)
        return redirect('/document/info/'+str(document_id)+'/')

def document_revisions(request, document_id):
    revisions = models.Revision.objects.filter(revision_document__pk=document_id)
    for revision in revisions:
        revision.signature_status = revision.get_signature()
    #pdb.set_trace()
    return render(request, 'document_revisions.html',{
        'revisions': revisions,
        'document_id': document_id,
        'nbar': 'documents'
    })

def document_shared(request):
    if request.user.is_authenticated:
        current_user = request.user
        user_shared_documents = current_user.document_set.all()
        return render(request,'document_shared.html',{
            'shared_documents':user_shared_documents,
            'nbar': 'shared'
        })
    else:
        return redirect("/accounts/login/")
