from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core.files import File

from document import views
from document.models import Document, Revision

class TestDocumentPage(TestCase):

    def setUp(self):
        self.user1 = User.objects.create_user('test1','test1@test1.com','test1test1')
        self.user2 = User.objects.create_user('test2','test2@test2.com','test2test2')
        self.document = Document(
            document_name = 'Unit Test Document',
            document_user = self.user1
        )
        self.document.save()


    def test_document_url_resolves_to_document_view(self):
        """ The /document/ url must resolve to document function """
        found = resolve('/document/')
        self.assertEqual(found.func, views.document)

    def test_document_returns_valid_html(self):
        """ The home page view must return correct HTML code """
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/')
        self.assertContains(response,'Document')

    def test_document_create_url_resolves_to_document_view(self):
        """ The /document/create/ url must resolve to the document_create view """
        found = resolve('/document/create/')
        self.assertEqual(found.func, views.document_create)

    def test_document_create_returns_valid_html(self):
        """ The document create view must return valid HTML code """
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/create/')
        self.assertContains(response,'Create Document')

    def test_document_create_post_request(self):
        """ A proper POST request to the /document/create/ page
            must create a new document object """
        self.client.login(username='test1',password='test1test1')
        response = self.client.post(
            '/document/create/',
            {
                'document_name':'Test Document 1',
                'document_workflow':'C'
            }
        )

        self.assertEqual(Document.objects.count(),2)
        self.assertRedirects(response,'/document/')
        response = self.client.get('/document/')
        self.assertContains(response,'Test Document 1')

    def test_revision_create_url_resolves_to_revision_create_view(self):
        """ The /document/add_revision/ url must resolve to the add revision view """
        found = resolve('/document/add_revision/'+str(self.document.pk)+'/')
        self.assertEqual(found.func, views.add_revision)

    def test_revision_create_returns_valid_html(self):
        """ The add revision view must return valid HTML code """
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/add_revision/'+str(self.document.pk)+'/')
        self.assertContains(response, 'Create Revision')

    def test_document_delete_url_resolves_to_document_delete_view(self):
        """ The /document/delete/id_document url must resolve
        to the document_delete view """
        found = resolve('/document/delete/'+str(self.document.pk)+'/')
        self.assertEqual(found.func, views.document_delete)

    def test_document_deletes_document(self):
        """ The document delete view must return valid HTML code """
        current_document_count = Document.objects.count()
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/delete/'+str(self.document.pk)+'/')
        self.assertEqual(Document.objects.count(), current_document_count - 1)

    def test_document_info_url_resolves_to_document_info_view(self):
        """ The /document/info/id_document url must resolve
        to the document_create view """
        found = resolve('/document/info/'+str(self.document.pk)+'/')
        self.assertEqual(found.func, views.document_info)

    def test_document_info_returns_valid_html(self):
        """ The document info view must return valid HTML code """
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/info/'+str(self.document.pk)+'/')
        self.assertContains(response, 'Document Information')

    def test_document_info_return_correct_last_revision(self):
        """ The document info must contain latest revision download link,
        else an error message with a link must be returned """
        self.client.login(username='test1',password='test1test1')
        response = self.client.get('/document/info/'+str(self.document.pk)+'/')
        self.assertContains(response, 'No revisions found')

    def test_document_share(self):
        self.document.send_to(self.user2)
        self.assertEqual(self.user2.document_set.all().count(),1)
        self.assertIn(self.document, self.user2.document_set.all())

    def test_document_check_out(self):
        self.document.check_out(self.user1)

        self.assertEqual(self.document.document_is_checked_out, True)

    def test_document_check_in(self):
        self.document.check_in(self.user1)

        self.assertEqual(self.document.document_is_checked_out, False)

    def test_document_authenticated_check_out_and_check_in(self):
        self.client.login(username='test1',password='test1test1')
        self.client.get('/document/check_out/'+str(self.document.pk)+'/')

        self.document = Document.objects.get(pk = self.document.pk)
        self.assertEqual(self.document.document_is_checked_out, True)
        self.client.get('/document/check_in/'+str(self.document.pk)+'/')
        self.document = Document.objects.get(pk = self.document.pk)
        self.assertEqual(self.document.document_is_checked_out, False)

    def test_document_unauthenticated_check_out_and_check_in(self):
        response = self.client.get('/document/check_out/'+str(self.document.pk)+'/')
        self.document = Document.objects.get(pk = self.document.pk)
        self.assertEqual(self.document.document_is_checked_out, False)
        self.assertEqual(response.status_code,403)
        response = self.client.get('/document/check_in/'+str(self.document.pk)+'/')
        self.document = Document.objects.get(pk = self.document.pk)
        self.assertEqual(self.document.document_is_checked_out, False)
        self.assertEqual(response.status_code,403)

    def test_revision_list_page(self):
        """ The /document/revisions/id_document/ url must resolve
        to the document_create view """
        found = resolve('/document/revisions/'+str(self.document.pk)+'/')
        self.assertEqual(found.func, views.document_revisions)
        response = self.client.get('/document/revisions/'+str(self.document.pk)+'/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'document_revisions.html')

    def test_document_shared_page(self):
        """ The /document/shared url must resolve
        to the shared documents view """
        self.client.login(username='test1',password='test1test1')
        found = resolve('/document/shared/')
        self.assertEqual(found.func, views.document_shared)
        response = self.client.get('/document/shared/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'document_shared.html')
