from django.contrib import admin

from .models import Document,Revision,Signature,DocumentComment

admin.site.register(Document)
admin.site.register(Revision)
admin.site.register(Signature)
admin.site.register(DocumentComment)
