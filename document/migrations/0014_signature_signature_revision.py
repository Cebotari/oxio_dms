# Generated by Django 2.0.3 on 2018-05-10 08:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0013_auto_20180510_0848'),
    ]

    operations = [
        migrations.AddField(
            model_name='signature',
            name='signature_revision',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='document.Revision'),
            preserve_default=False,
        ),
    ]
