# Generated by Django 2.0.3 on 2018-04-09 19:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0008_documentcomment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentcomment',
            name='comment_contents',
            field=models.TextField(max_length=300),
        ),
    ]
