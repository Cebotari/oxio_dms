from itertools import chain
import gnupg
import base64
import logging
import pdb

from django.db import models
from django.contrib.auth.models import User

from document import crypto_settings
from document import workflow

CREATE_ACTION ='C'
READ_ACTION = 'R'
DELETE_ACTION = 'D'
CHECK_IN_ACTION ='I'
CHECK_OUT_ACTION = 'O'

class Document(models.Model):
    WORKFLOW_CHOICES = (
        ( workflow.COLLABORATION_WORKFLOW, "Collaboration" ),
        ( workflow.APPROVAL_WORKFLOW, "Approval"),
    )
    STATE_CHOICES = (
        (workflow.PRIVATE_STATE,"Private Document"),
        (workflow.COLLABORATION_STATE, "Collaboration"),
        (workflow.ARCHIVED_STATE, "Archived Document"),
        (workflow.SENT_FOR_APPROVAL_STATE, "Sent For Approval"),
        (workflow.APPROVED_STATE, "Approved"),
        (workflow.RETURNED_STATE,"Returned Document")
    )
    document_name = models.CharField( max_length=300 )
    document_creation_date = models.DateTimeField(auto_now = True)
    document_user = models.ForeignKey(
        User, on_delete = models.CASCADE,
        related_name='document_owner_rel'
    )
    document_is_checked_out = models.BooleanField(default = False)
    document_shared_to_users = models.ManyToManyField(User, related_name = 'document_set')
    document_workflow = models.CharField(
        max_length=1,
        choices = WORKFLOW_CHOICES,
        default = workflow.COLLABORATION_WORKFLOW
    )
    document_state = models.CharField(
        max_length = 1,
        choices = STATE_CHOICES,
        default = workflow.PRIVATE_STATE
    )

    def send_to(self, user):
        return self.document_shared_to_users.add(user)

    def last_revision_download(self):
        try:
            last_revision = Revision.objects.filter(revision_document = self).order_by('-id')[0]
            return last_revision.revision_contents.url
        except IndexError:
            return False

    def get_comments(self):
        return DocumentComment.objects.filter(comment_document = self)

    def check_in(self, user):
        self.document_is_checked_out = False
        self.save()

    def check_out(self, user):
        self.document_is_checked_out = True
        self.save()

    def get_allowed_users(self):
        shared_users = self.document_shared_to_users.all()
        return chain((self.document_user,), shared_users)

    def __str__(self):
        return self.document_name


class Revision(models.Model):
    revision_document = models.ForeignKey(Document, on_delete = models.CASCADE)
    revision_time = models.DateTimeField(auto_now = True)
    revision_contents = models.FileField(upload_to="document_storage/")
    revision_signature = models.FileField(upload_to="signature_storage/", blank=True)
    revision_user = models.ForeignKey(User,on_delete = models.CASCADE)

    def verify_signature(self):
        gpg = gnupg.GPG(gnupghome=crypto_settings.GPG_HOME)
        file_location = self.revision_contents.path
        signature_location = open(self.revision_signature.path,'rb')
        result = gpg.verify_file(signature_location, file_location)
        logging.info(result.trust_level)
        if result.trust_level != None:
            current_signature = Signature(
                signature_revision = self,
                signature_username = result.username,
                signature_key_id = result.key_id,
                signature_id = result.signature_id,
                signature_fingerprint = result.fingerprint,
                signature_trust_level = result.trust_level,
                signature_trust_text = result.trust_text
            )
            current_signature.save()
            self.save()
            return True
        else:
            return False

    def get_signature(self):
        try:
            current_signature = Signature.objects.get(signature_revision__pk = self.pk)
        except Signature.DoesNotExist:
            current_signature = None
        return current_signature
    
    def save(self):
        new_log = DocumentLog(
            document_log_user_login = self.revision_user,
            document_log_document_name = self.revision_document,
            document_log_revision = self,
            document_log_action = CREATE_ACTION
        )
        new_log.save()
        return super().save()
            

class Signature(models.Model):
    signature_username = models.TextField()
    signature_key_id = models.TextField()
    signature_id = models.TextField()
    signature_fingerprint = models.TextField()
    signature_trust_level = models.TextField()
    signature_trust_text = models.TextField()
    signature_revision = models.OneToOneField(Revision, on_delete = models.CASCADE)


class DocumentLog(models.Model):
    ACTION_CHOICES = (
        (CREATE_ACTION, 'Create'),
        (READ_ACTION, 'Read'),
        (DELETE_ACTION, 'Delete'),
        (CHECK_IN_ACTION, 'Check-In'),
        (CHECK_OUT_ACTION, 'Check-Out')
    )

    document_log_user_login = models.CharField( max_length = 300 )
    document_log_document_name = models.CharField( max_length = 300 )
    document_log_revision = models.CharField( max_length = 300 )
    document_log_time = models.DateTimeField(auto_now = True)
    document_log_action = models.TextField(max_length = 1, choices = ACTION_CHOICES)

class DocumentComment(models.Model):
    comment_contents = models.TextField(max_length = 300)
    comment_document = models.ForeignKey(Document, on_delete = models.CASCADE)
    comment_date = models.DateTimeField(auto_now = True)
    comment_user = models.ForeignKey(User, on_delete=models.CASCADE)
