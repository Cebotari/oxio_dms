
COLLABORATION_STATE = "C"
COLLABORATION_WORKFLOW = "C"
APPROVAL_WORKFLOW = "A"
PRIVATE_STATE = "P"
ARCHIVED_STATE = "R"
SENT_FOR_APPROVAL_STATE = "S"
APPROVED_STATE = "V"
RETURNED_STATE = "E"

class Workflow():

    def get_state(self,state_name):
        if state_name == COLLABORATION_STATE :
            return CollaborationState()
        elif state_name == PRIVATE_STATE :
            return PrivateState()
        elif state_name == ARCHIVE_STATE :
            return ArchiveState()
        elif state_name == SENT_FOR_APPROVAL_STATE :
            return SentForApprovalState()
        elif state_name == APPROVED_STATE :
            return ApprovedState()
        elif state_name == RETURNED_STATE :
            return ReturnedState()
        else:
            return None

class CollaborationWorkflow():

    def __init__(self):
        self.states = [
            PRIVATE_STATE,
            COLLABORATION_STATE,
            ARCHIVED_STATE
        ]
        self.initial_state = self.states[0]
        self.final_state = self.states[2]
        self.retention_period = 180

class ApprovalWorkflow():

    def __init__(self):
        self.states = [
            PRIVATE_STATE,
            SENT_FOR_APPROVAL_STATE,
            APPROVED_STATE,
            RETURNED_STATE,
            ARCHIVED_STATE
        ]
        self.initial_state = self.states[0]
        self.final_state = self.states[4]
        self.retention_period = 180


class WorkflowState():

    def to_private_document_state(self, document):
        raise NotImplementedError

    def to_archive_state(self, document):
        raise NotImplementedError

    def to_collaboration_document_state(self, document):
        raise NotImplementedError

    def to_return_document_state(self, document):
        raise NotImplementedError

    def to_send_to_approval_state(self, document):
        raise NotImplementedError

    def to_approve_document_state(self, document):
        raise NotImplementedError

class PrivateDocumentState(WorkflowState):

    def __init__(self):
        self.next_states = [
            COLLABORATION_STATE,
            SENT_TO_APPROVAL_STATE
        ]

    def to_private_document_state(self, document):
        pass

    def to_collaboration_document_state(self, document):
        if document.document_workflow.__class__.__name__ == 'COLLABORATION_WORKFLOW':
            document.document_state = CollaborationState()
            document.save()

    def to_send_to_approval_state(self, document):
        if document.document_workflow.__class__.__name__ == 'APPROVAL_WORKFLOW':
            document.document_state = SentToApprovalState()
            document.save()

class CollaborationState(WorkflowState):

    def __init__(self):
        self.next_states = [ARCHIVED_STATE]

    def to_collaboration_document_state(self, document):
        pass

    def to_archive_state(self, document):
        pass

class ArchiveState(WorkflowState):

    def __init__(self):
        self.next_states = None

    def to_archive_state(self, document):
        pass


class SentToApprovalState(WorkflowState):

    def __init__(self):
        self.next_states = [
            APPROVED_STATE,
            RETURNED_STATE,
            ARCHIVED_STATE
        ]

    def to_approve_document_state(self, document):
        pass

    def to_returned_document_state(self, document):
        pass

    def to_archive_state(self, document):
        pass

class ApprovedDocumentState(WorkflowState):

    def __init__(self):
        self.next_states = [
            ARCHIVED_STATE
        ]

    def to_archive_state(self, document):
        pass

class ReturnedDocumentState(WorkflowState):

    def __init__(self):
        self.next_states = [
            SENT_TO_APPROVAL_STATE
        ]

    def to_approve_document_state(self, document):
     pass
