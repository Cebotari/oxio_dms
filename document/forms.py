from django import forms
from django.contrib.auth.models import User

from . import models

class CreateDocumentForm(forms.ModelForm):
    class Meta:
        model = models.Document
        fields = ('document_name','document_workflow')

class CreateRevisionForm(forms.ModelForm):
    class Meta:
        model = models.Revision
        fields = ('revision_contents','revision_signature')

class DocumentShareForm(forms.Form):
    user_field = forms.ModelMultipleChoiceField(User.objects.all())

class AddCommentForm(forms.ModelForm):
    class Meta:
        model = models.DocumentComment
        fields = ('comment_contents',)
