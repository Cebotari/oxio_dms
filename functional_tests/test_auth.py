from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import LiveServerTestCase

import time

class NewVisitorTest (LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self,row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_user_signup_and_login(self):
        # Elena needs to use Oxio DMS for her job. She goes
        # to check out its homepage
        self.browser.get(self.live_server_url)

        # She notices the page title and header mention Oxio DMS
        self.assertIn('Home Page', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Oxio DMS', header_text)

        # She notices a sign-up button
        signup_button = self.browser.find_element_by_id('id_sign_up_button')
        signup_button.click()

        # She is redirected to a new page - sign-up
        # where she enters a username and a password
        username_field = self.browser.find_element_by_id('id_username')
        password1_field = self.browser.find_element_by_id('id_password1')
        password2_field = self.browser.find_element_by_id('id_password2')
        sign_up_button = self.browser.find_element_by_id('sign_up_button')

        # She fills in the details for a new user and clicks on the "Sign Up" button
        username_field.send_keys('test1')
        password1_field.send_keys('test1test1')
        password2_field.send_keys('test1test1')
        sign_up_button.click()

        # We have been redirected to the Log In page. We can Log in using the details above
        self.assertIn('Log In',self.browser.find_element_by_tag_name('h1').text)
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        login_button = self.browser.find_element_by_id('login_button')

        # The data is entered to the fields and the login button is clicked
        username_field.send_keys('test1')
        password_field.send_keys('test1test1')
        login_button.click()
        # She is welcomed in the header of the application
        self.assertIn('Welcome, test1!',self.browser.find_element_by_tag_name("h1").text)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
