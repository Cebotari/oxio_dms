from selenium import webdriver
from django.contrib.auth.models import User

from functional_tests.tests import LoggedInUserTest
from profiles.models import Key

import time

class KeyManagementTest(LoggedInUserTest):

    def test_adding_key(self):
        self.log_me_in('test1','test1test1')
        self.browser.get(self.live_server_url+'/accounts/profile/')
        add_key_button = self.browser.find_element_by_id('menu_add_key_button')
        add_key_button.click()
        key_name_field = self.browser.find_element_by_id('id_key_name')
        key_contents_field = self.browser.find_element_by_id('id_key_contents')
        submit_button = self.browser.find_element_by_id('id_key_submit_button')
        key_name_field.send_keys("Test key 1")
        key_contents_field.send_keys("""-----BEGIN PGP PUBLIC KEY BLOCK-----
        mQENBFrBOmcBCADiyepM4TXBcWxNjrTDsSWDWdKVu1Z+P865EIfWhKGyZkbfNbFa
        vfM+W15wCelGF2STZxd82HkY9RLSfaZxcPiblBPdl45dKqCXVksiPp4ENYdDQVf6
        MUntcQS/wF1xFyImS6aRL55wj5unZTDzJXbDyN9ZLqUqlNQibJ7ApqL9x0VT44QN
        SFlX3R2rtjvrsJ3SaKCvzZ4zH+1bYwwebZ1AaEFIhJWd2cZXKVeqDJ8H1QgRx5Yg
        hVX+3Osv6tCBmPIJEG1Z9MK4KMIFWXRZMl2zJImTeztwJ/Bs2l3Ow/aMUKRjgp1n
        kmaX5Qh6OvPRAFJ4WHEPx7UUqYn8p4nQ+whbABEBAAG0O0lvbiBDZWJvdGFyaSAo
        VGVzdCBLZXkgZm9yIE94aW9fRE1TKSA8YW5hdGhlbWl6YXR1QG1haWwucnU+iQE3
        BBMBCAAhBQJawTpnAhsDBQsJCAcCBhUICQoLAgQWAgMBAh4BAheAAAoJEPDfhP4n
        nz3joAMH/jXf/Cl0Oy0kldI1Ce5cE8laRJ1Hsju+iatqLrGfl/ig75GOz2hmnOhY
        7kmmOTBh7njzj90cwuDPnl7rCzYKI9E7DhKlOLBWYl50L1CXoJurCV2vgLCM9ND+
        ilk8GMiegpTFaLUeLDHIdKoUMTnBF7IdYRnhXuejENjT8HVWkGMjMqvAKZA297dn
        0diJ130nVLdYrObQsDtXyAApcPhsB60VC4MbtuwzVreQ+REyGWdeOeUkPHWycsN5
        arjNPRVxAVbSzo3GEngoFEEtjNNZVpQjwgvb87WRPFyjsDjCPLkTkrGU0rIp04Kk
        RFr9Lw9ZHiJfBltJF3bGI64tiGO0Gp65AQ0EWsE6ZwEIAJPsRl7WcIa3Vo/80p3I
        lXlIuSw0G6yaecLAYZ8icB+cXb6AH0wFedIOchai2Mvg/E2JApD7+GNoz7mXQ3Ik
        rkbjQy9c199puc7SRCqmA5rwqabs6tohoC7HdtWWlQvg3LLIpkxsbKZAsIYG6P05
        l1wPPNybqTgpKiJGcG40HSC+nRQSqDQXNQUCb6PuW9k6tI79VIAnt1KN4qPibEXy
        ndakyuWzrGQOCS6V8+ciWCMwcB+1DCMnNbTCidC2ELGhdlJJa/p/vMPkcgoduChj
        0n8U7iRFCbm+w4AFzm0aNAnXAEhCmE50uETKJMTh6bw8Yi0owoeZGiVUF5TavV8f
        VncAEQEAAYkBHwQYAQgACQUCWsE6ZwIbDAAKCRDw34T+J5894zgVCAC6QNSbdHcP
        gm3ONtqIWzUo6VoFs5V0uV01XrIZ1zO6TSOQKBYLMW3W0CumaaCf1bGiNsImd5fA
        1x2l1KEWGAtyBZw6F2oyORQmvbx1VqP/FQXdnrrh91uR6yCACS6sZNCS8fTfOXDL
        51fhJP48BUfRMHE5UybEjkxL5qOb2x7n4DVXaTzR3hqOYtdlS915QGIFXv23Ja8d
        ImBVYrfxEyMd5aYznAa3QGxdiTExW3pkEwiRWJ+4hHfXoy6bQC8Vp0Mgh5VXa3Pb
        yiPLTtYw34blkwFRPKu7adBNx1er6j/0I8gSM3AoO+xbouH4+cnjJR9jCQ1H0LWX
        DLy9EGbRMyCc
        =XRRh
        -----END PGP PUBLIC KEY BLOCK-----""")
        submit_button.click()
        self.assertEqual(self.browser.current_url, self.live_server_url+'/keys/')
        self.assertIn('Test key 1',self.browser.find_element_by_id('keys_table').text)
        self.added_key = Key.objects.latest('pk')
        self.assertEqual(self.added_key.key_fingerprints,'98F467EB37AAA999DC3C4B9FF0DF84FE279F3DE3')
        self.added_key.delete()

    def test_deleting_key(self):
        deleted_key = Key(
            key_name = 'Test Key1',
            key_contents = 'Key Contents1',
            key_user = self.user1
        )
        deleted_key.save()

        # Accessing the keys page and finding the delete key link
        # The class is used because many links cand be present on a single page
        self.log_me_in('test1','test1test1')
        self.browser.get(self.live_server_url+'/keys/')
        self.assertIn('Test Key1',self.browser.find_element_by_id('keys_table').text)
        key_delete_button = self.browser.find_element_by_class_name('key_delete')

        # A correctly deleted key redirects the user to the /keys/ page
        key_delete_button.click()
        self.assertEqual(self.browser.current_url,self.live_server_url+'/keys/')
        # The Test key1 should be absent from the page
        self.assertNotIn('Test Key1',self.browser.find_element_by_id('keys_table').text)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
