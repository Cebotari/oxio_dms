from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import LiveServerTestCase
from django.contrib.auth.models import User

from profiles.models import Key


import time

class NotLoggedInRedirectTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()


    def test_profile_redirect(self):
        # Any user that accesses the page "/accounts/profile/ unauthenticated 
        # is redirected to the login page
        self.browser.get(self.live_server_url+'/accounts/profile/')
        self.assertEqual(self.live_server_url+'/accounts/login/',self.browser.current_url)

    def test_keys_redirect(self):
        # Any user that accesses the page "/accounts/profile/ unauthenticated 
        # is redirected to the login page
        self.browser.get(self.live_server_url+'/keys/')
        self.assertEqual(self.live_server_url+'/accounts/login/',self.browser.current_url)

    def test_documents_redirect(self):
        # Any user that accesses the page "/accounts/profile/ unauthenticated 
        # is redirected to the login page
        self.browser.get(self.live_server_url+'/document/')
        self.assertEqual(self.live_server_url+'/accounts/login/',self.browser.current_url)

class LoggedInUserTest(LiveServerTestCase):

    def setUp(self):
        self.user1 = User.objects.create_user('test1',None,'test1test1')
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(5)

    def tearDown(self):
        self.browser.quit()

    def log_me_in(self, username, password):
        self.browser.get(self.live_server_url+'/accounts/login/')
        self.assertIn('Log In',self.browser.find_element_by_tag_name('h1').text)
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        login_button = self.browser.find_element_by_id('login_button')
        username_field.send_keys(username)
        password_field.send_keys(password)
        login_button.click()



if __name__ == '__main__':
    unittest.main(warnings='ignore')
