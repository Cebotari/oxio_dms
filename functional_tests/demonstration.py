import django
from selenium import webdriver
from django.contrib.auth.models import User

from functional_tests.tests import LoggedInUserTest
from document.models import Document, Revision
from profiles.models import Key

import time

class Demonstration(LoggedInUserTest):
    def setUp(self):
       self.user1 = User.objects.create_user('test1',None,'test1test1')
       self.browser = webdriver.Firefox()
       self.browser.implicitly_wait(5)
       self.user2 = User.objects.create_user('test2',None,'test1test1')
       self.user3 = User.objects.create_user('test3',None,'test1test1')


    def tearDown(self):
        self.browser.quit()

    def test_demonstration(self):
        self.browser.get(self.live_server_url)
        time.sleep(4)
        self.browser.get(self.live_server_url+'/accounts/login/')
        time.sleep(4)
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        login_button = self.browser.find_element_by_id('login_button')
        username_field.send_keys('test1')
        password_field.send_keys('test1test1')
        login_button.click()
        time.sleep(4)
        self.browser.get(self.live_server_url+'/accounts/profile/')
        add_key_button = self.browser.find_element_by_id('menu_add_key_button')
        add_key_button.click()
        time.sleep(4)
        key_name_field = self.browser.find_element_by_id('id_key_name')
        key_contents_field = self.browser.find_element_by_id('id_key_contents')
        submit_button = self.browser.find_element_by_id('id_key_submit_button')
        key_name_field.send_keys("Test key 1")
        key_contents_field.send_keys("""-----BEGIN PGP PUBLIC KEY BLOCK-----
        mQENBFrBOmcBCADiyepM4TXBcWxNjrTDsSWDWdKVu1Z+P865EIfWhKGyZkbfNbFa
        vfM+W15wCelGF2STZxd82HkY9RLSfaZxcPiblBPdl45dKqCXVksiPp4ENYdDQVf6
        MUntcQS/wF1xFyImS6aRL55wj5unZTDzJXbDyN9ZLqUqlNQibJ7ApqL9x0VT44QN
        SFlX3R2rtjvrsJ3SaKCvzZ4zH+1bYwwebZ1AaEFIhJWd2cZXKVeqDJ8H1QgRx5Yg
        hVX+3Osv6tCBmPIJEG1Z9MK4KMIFWXRZMl2zJImTeztwJ/Bs2l3Ow/aMUKRjgp1n
        kmaX5Qh6OvPRAFJ4WHEPx7UUqYn8p4nQ+whbABEBAAG0O0lvbiBDZWJvdGFyaSAo
        VGVzdCBLZXkgZm9yIE94aW9fRE1TKSA8YW5hdGhlbWl6YXR1QG1haWwucnU+iQE3
        BBMBCAAhBQJawTpnAhsDBQsJCAcCBhUICQoLAgQWAgMBAh4BAheAAAoJEPDfhP4n
        nz3joAMH/jXf/Cl0Oy0kldI1Ce5cE8laRJ1Hsju+iatqLrGfl/ig75GOz2hmnOhY
        7kmmOTBh7njzj90cwuDPnl7rCzYKI9E7DhKlOLBWYl50L1CXoJurCV2vgLCM9ND+
        ilk8GMiegpTFaLUeLDHIdKoUMTnBF7IdYRnhXuejENjT8HVWkGMjMqvAKZA297dn
        0diJ130nVLdYrObQsDtXyAApcPhsB60VC4MbtuwzVreQ+REyGWdeOeUkPHWycsN5
        arjNPRVxAVbSzo3GEngoFEEtjNNZVpQjwgvb87WRPFyjsDjCPLkTkrGU0rIp04Kk
        RFr9Lw9ZHiJfBltJF3bGI64tiGO0Gp65AQ0EWsE6ZwEIAJPsRl7WcIa3Vo/80p3I
        lXlIuSw0G6yaecLAYZ8icB+cXb6AH0wFedIOchai2Mvg/E2JApD7+GNoz7mXQ3Ik
        rkbjQy9c199puc7SRCqmA5rwqabs6tohoC7HdtWWlQvg3LLIpkxsbKZAsIYG6P05
        l1wPPNybqTgpKiJGcG40HSC+nRQSqDQXNQUCb6PuW9k6tI79VIAnt1KN4qPibEXy
        ndakyuWzrGQOCS6V8+ciWCMwcB+1DCMnNbTCidC2ELGhdlJJa/p/vMPkcgoduChj
        0n8U7iRFCbm+w4AFzm0aNAnXAEhCmE50uETKJMTh6bw8Yi0owoeZGiVUF5TavV8f
        VncAEQEAAYkBHwQYAQgACQUCWsE6ZwIbDAAKCRDw34T+J5894zgVCAC6QNSbdHcP
        gm3ONtqIWzUo6VoFs5V0uV01XrIZ1zO6TSOQKBYLMW3W0CumaaCf1bGiNsImd5fA
        1x2l1KEWGAtyBZw6F2oyORQmvbx1VqP/FQXdnrrh91uR6yCACS6sZNCS8fTfOXDL
        51fhJP48BUfRMHE5UybEjkxL5qOb2x7n4DVXaTzR3hqOYtdlS915QGIFXv23Ja8d
        ImBVYrfxEyMd5aYznAa3QGxdiTExW3pkEwiRWJ+4hHfXoy6bQC8Vp0Mgh5VXa3Pb
        yiPLTtYw34blkwFRPKu7adBNx1er6j/0I8gSM3AoO+xbouH4+cnjJR9jCQ1H0LWX
        DLy9EGbRMyCc
        =XRRh
        -----END PGP PUBLIC KEY BLOCK-----""")
        submit_button.click()
        time.sleep(9)
        self.browser.get(self.live_server_url+'/accounts/profile/')
        documents_button = self.browser.find_element_by_id('menu_documents_button')
        documents_button.click()
        time.sleep(4)
        self.assertEqual(self.browser.current_url,self.live_server_url+'/document/')
        self.assertEqual(self.browser.title,'Document')
        page_title = self.browser.find_element_by_tag_name("h1").text
        self.assertEqual(page_title,'Document')
        new_document_button = self.browser.find_element_by_id('new_document_button')
        new_document_button.click()
        time.sleep(4)
        self.assertEqual(self.browser.current_url,self.live_server_url+'/document/create/')
        self.assertEqual(self.browser.title,'Create Document')
        page_title = self.browser.find_element_by_tag_name("h1").text
        self.assertEqual(page_title,'Create Document')
        document_name_field = self.browser.find_element_by_id('id_document_name')
        document_submit_button = self.browser.find_element_by_id('document_submit_button_id')
        document_name_field.send_keys('Demo UTM')
        document_submit_button.click()
        time.sleep(4)
        new_document_name = self.browser.find_element_by_class_name('document_name').text
        self.assertEqual(new_document_name,'Demo UTM')
        self.browser.get(self.live_server_url+'/document/')
        revision_button = self.browser.find_element_by_class_name('document_revision')
        revision_button.click()
        time.sleep(4)
        revision_contents_field = self.browser.find_element_by_id('id_revision_contents')
        revision_signature_field = self.browser.find_element_by_id('id_revision_signature')
        revision_submit_button = self.browser.find_element_by_id('id_revision_submit_button')
        revision_contents_field.send_keys('/home/ion/oxio_dms/functional_tests/files/test1.odt')
        revision_signature_field.send_keys('/home/ion/oxio_dms/functional_tests/files/test1.odt.sig')
        revision_submit_button.click()
        time.sleep(9)
        self.assertEqual(self.browser.current_url,self.live_server_url+'/document/info/1/')
        latest_revision = self.browser.find_element_by_id('id_latest_revision').text
        self.assertEqual(latest_revision, 'Last Revision: Download')

        self.browser.get(self.live_server_url+'/document/info/1/')
        revision_list_button = self.browser.find_element_by_id('id_document_revision_list')
        revision_list_button.click()
        time.sleep(4)
        self.browser.get(self.live_server_url+'/document/')
        share_button = self.browser.find_element_by_class_name('document_share')
        share_button.click()
        time.sleep(4)
        select_user_field = self.browser.find_element_by_id('id_user_field')
        share_button = self.browser.find_element_by_id('document_share_button_id')
        select_plugin = webdriver.support.select.Select(select_user_field)
        select_plugin.select_by_value(str(self.user2.pk))
        share_button.click()
        time.sleep(4)
        shared_user = self.browser.find_element_by_class_name('users_shared')
        self.assertEqual(shared_user.text,'test2')
        logout_button = self.browser.find_element_by_id('menu_logout_button')
        logout_button.click()
        time.sleep(4)
        self.browser.get(self.live_server_url+'/accounts/login/')
        time.sleep(4)
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        login_button = self.browser.find_element_by_id('login_button')
        username_field.send_keys('test2')
        password_field.send_keys('test1test1')
        login_button.click()
        self.browser.get(self.live_server_url+'/document/shared/')
        time.sleep(4)
        self.browser.get(self.live_server_url+'/document/info/1/')
        comment_field = self.browser.find_element_by_id('id_comment_contents')
        comment_field.send_keys("UTM is the best!")
        comment_button = self.browser.find_element_by_id('add_comment_button_id')
        comment_button.click()
        time.sleep(7)
        logout_button = self.browser.find_element_by_id('menu_logout_button')
        logout_button.click()
        time.sleep(4)
        self.browser.get(self.live_server_url+'/accounts/login/')
        time.sleep(4)
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        login_button = self.browser.find_element_by_id('login_button')
        username_field.send_keys('test1')
        password_field.send_keys('test1test1')
        login_button.click()
        self.browser.get(self.live_server_url+'/document/info/1/')
        time.sleep(10)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
