"""oxio_dms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path,include
from django.contrib.auth import urls as auth_urls
from django.views.static import serve

from oxio_dms import settings
from profiles import views as profile_views
from document import views as document_views

urlpatterns = [
    path('', profile_views.home_page, name='home_page'),
    path('accounts/password_change/',profile_views.change_password,name='change_password'),
    path('accounts/', include(('django.contrib.auth.urls','accounts'))),
    path('accounts/signup/', profile_views.signup, name='signup'),
    path('accounts/profile/',profile_views.profile, name='profile'),
    path('admin/', admin.site.urls),
    path('keys/',profile_views.add_key),
    path('keys/delete/<int:key_id>/',profile_views.remove_key),
    path('document/', document_views.document),
    path('document/create/', document_views.document_create),
    path('document/info/<int:document_id>/',document_views.document_info),
    path('document/delete/<int:document_id>/',document_views.document_delete),
    path('document/add_revision/<int:document_id>/',document_views.add_revision),
    path('document/revisions/<int:document_id>/',document_views.document_revisions),
    path('document/share/<int:document_id>/',document_views.document_share),
    path('document/add_comment/<int:document_id>/',document_views.add_comment),
    path('document/check_out/<int:document_id>/',document_views.document_check_out),
    path('document/check_in/<int:document_id>/',document_views.document_check_in),
    path('document/shared/', document_views.document_shared),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^document_storage/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
